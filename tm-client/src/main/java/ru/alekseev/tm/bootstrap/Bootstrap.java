package ru.alekseev.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.alekseev.tm.api.ITerminalService;
import ru.alekseev.tm.api.ServiceLocator;
import ru.alekseev.tm.api.iendpoint.RoleType;
import ru.alekseev.tm.api.iendpoint.Session;
import ru.alekseev.tm.api.iendpoint.SessionDTO;
import ru.alekseev.tm.command.system.AbstractCommand;
import ru.alekseev.tm.endpoint.*;
import ru.alekseev.tm.service.TerminalService;

import java.util.*;

public final class Bootstrap implements ServiceLocator {
    @Nullable private SessionDTO currentSession;

    @Override
    public SessionDTO getCurrentSession() {
        return currentSession;
    }

    @Override
    public void setCurrentSession(SessionDTO currentSession) {
        this.currentSession = currentSession;
    }

    @NotNull private final ProjectEndpointService projectEndpointService = new ProjectEndpointService();
    @NotNull private final TaskEndpointService taskEndpointService = new TaskEndpointService();
    @NotNull private final UserEndpointService userEndpointService = new UserEndpointService();
    @NotNull private final SessionEndpointService sessionEndpointService = new SessionEndpointService();
    @NotNull private final DomainEndpointService domainEndpointService = new DomainEndpointService();

    @NotNull private final ITerminalService terminalService = new TerminalService();

    @NotNull private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @Nullable private final Set<Class<? extends AbstractCommand>> classes =
            new Reflections("ru.alekseev.tm").getSubTypesOf(AbstractCommand.class);

    @Override
    @NotNull
    public final List<AbstractCommand> getCommands() {
        return new ArrayList<>(commands.values());
    }

    @Override
    @NotNull
    public DomainEndpointService getDomainEndpointService() {
        return domainEndpointService;
    }

    @Override
    @NotNull
    public final ProjectEndpointService getProjectEndpointService() {
        return projectEndpointService;
    }

    @Override
    @NotNull
    public final TaskEndpointService getTaskEndpointService() {
        return taskEndpointService;
    }

    @Override
    @NotNull
    public final UserEndpointService getUserEndpointService() {
        return userEndpointService;
    }

    @Override
    @NotNull
    public final SessionEndpointService getSessionEndpointService() {
        return sessionEndpointService;
    }

    @Override
    @NotNull
    public final ITerminalService getTerminalService() {
        return terminalService;
    }

    public final void registry(@NotNull final AbstractCommand command) {
        @NotNull final String commandName = command.getName();
        commands.put(commandName, command);
    }

    public final void start() throws Exception {
        if (classes == null) return;
        initCommands(classes);
        System.out.println("*** WELCOME TO TASK MANAGER ***");

        while (true) {
            @NotNull String commandName = terminalService.getFromConsole();
            if (!commands.containsKey(commandName)) {
                commandName = "help";
            }

            if (commands.get(commandName).isSecure() && currentSession == null) {
                System.out.println("LOGIN INTO PROJECT MANAGER");
                System.out.println();
                continue;
            }

            if (commands.get(commandName).isForAdminOnly() && userEndpointService.getUserEndpointPort().findOneUser(currentSession).getRoleType() != RoleType.ADMIN) {
                System.out.println("ACCESS DENIED. YOU DON'T HAVE ADMINISTRATOR RIGHTS");
                System.out.println();
                continue;
            }

            try {
                commands.get(commandName).execute();
            } catch (@NotNull Exception e) {
                System.out.println("An exception was thrown");
                e.printStackTrace();
            }
            System.out.println();
        }
    }

    public final void initCommands(@NotNull final Set<Class<? extends AbstractCommand>> classes) throws Exception {
        for (@Nullable final Class commandClass : classes) {
            if (commandClass == null) continue;
            if (AbstractCommand.class.isAssignableFrom(commandClass)) {
                @NotNull final AbstractCommand command = (AbstractCommand) commandClass.newInstance();
                command.setServiceLocator(this);
                registry(command);
            }
        }
    }

}