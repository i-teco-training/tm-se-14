package ru.alekseev.tm.api.iendpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.2.7
 * 2019-11-27T10:39:48.147+03:00
 * Generated source version: 3.2.7
 *
 */
@WebService(targetNamespace = "http://iendpoint.api.tm.alekseev.ru/", name = "IDomainEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface IDomainEndpoint {

    @WebMethod
    @Action(input = "http://iendpoint.api.tm.alekseev.ru/IDomainEndpoint/setDomainRequest", output = "http://iendpoint.api.tm.alekseev.ru/IDomainEndpoint/setDomainResponse")
    @RequestWrapper(localName = "setDomain", targetNamespace = "http://iendpoint.api.tm.alekseev.ru/", className = "ru.alekseev.tm.api.iendpoint.SetDomain")
    @ResponseWrapper(localName = "setDomainResponse", targetNamespace = "http://iendpoint.api.tm.alekseev.ru/", className = "ru.alekseev.tm.api.iendpoint.SetDomainResponse")
    public void setDomain(
        @WebParam(name = "arg0", targetNamespace = "")
        ru.alekseev.tm.api.iendpoint.SessionDTO arg0,
        @WebParam(name = "arg1", targetNamespace = "")
        ru.alekseev.tm.api.iendpoint.Domain arg1
    );

    @WebMethod
    @Action(input = "http://iendpoint.api.tm.alekseev.ru/IDomainEndpoint/getDomainRequest", output = "http://iendpoint.api.tm.alekseev.ru/IDomainEndpoint/getDomainResponse")
    @RequestWrapper(localName = "getDomain", targetNamespace = "http://iendpoint.api.tm.alekseev.ru/", className = "ru.alekseev.tm.api.iendpoint.GetDomain")
    @ResponseWrapper(localName = "getDomainResponse", targetNamespace = "http://iendpoint.api.tm.alekseev.ru/", className = "ru.alekseev.tm.api.iendpoint.GetDomainResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.alekseev.tm.api.iendpoint.Domain getDomain(
        @WebParam(name = "arg0", targetNamespace = "")
        ru.alekseev.tm.api.iendpoint.SessionDTO arg0
    );
}
