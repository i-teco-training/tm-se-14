package ru.alekseev.tm.api;

import ru.alekseev.tm.api.iendpoint.Session;
import ru.alekseev.tm.api.iendpoint.SessionDTO;
import ru.alekseev.tm.command.system.AbstractCommand;
import ru.alekseev.tm.endpoint.*;

import java.util.List;

public interface ServiceLocator {

    SessionDTO getCurrentSession();

    void setCurrentSession(SessionDTO currentSession);

    //IService getService();

    ITerminalService getTerminalService();

    DomainEndpointService getDomainEndpointService();

    ProjectEndpointService getProjectEndpointService();

    TaskEndpointService getTaskEndpointService();

    UserEndpointService getUserEndpointService();

    SessionEndpointService getSessionEndpointService();

    List<AbstractCommand> getCommands();//????
}
