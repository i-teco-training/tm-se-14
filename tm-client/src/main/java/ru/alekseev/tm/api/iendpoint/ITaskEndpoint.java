package ru.alekseev.tm.api.iendpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.2.7
 * 2019-11-27T10:39:48.616+03:00
 * Generated source version: 3.2.7
 *
 */
@WebService(targetNamespace = "http://iendpoint.api.tm.alekseev.ru/", name = "ITaskEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface ITaskEndpoint {

    @WebMethod
    @Action(input = "http://iendpoint.api.tm.alekseev.ru/ITaskEndpoint/findOneTaskByUserIdAndTaskIdRequest", output = "http://iendpoint.api.tm.alekseev.ru/ITaskEndpoint/findOneTaskByUserIdAndTaskIdResponse")
    @RequestWrapper(localName = "findOneTaskByUserIdAndTaskId", targetNamespace = "http://iendpoint.api.tm.alekseev.ru/", className = "ru.alekseev.tm.api.iendpoint.FindOneTaskByUserIdAndTaskId")
    @ResponseWrapper(localName = "findOneTaskByUserIdAndTaskIdResponse", targetNamespace = "http://iendpoint.api.tm.alekseev.ru/", className = "ru.alekseev.tm.api.iendpoint.FindOneTaskByUserIdAndTaskIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.alekseev.tm.api.iendpoint.TaskDTO findOneTaskByUserIdAndTaskId(
        @WebParam(name = "arg0", targetNamespace = "")
        java.lang.String arg0,
        @WebParam(name = "arg1", targetNamespace = "")
        java.lang.String arg1
    );

    @WebMethod
    @Action(input = "http://iendpoint.api.tm.alekseev.ru/ITaskEndpoint/updateTaskByTaskIdTaskNameRequest", output = "http://iendpoint.api.tm.alekseev.ru/ITaskEndpoint/updateTaskByTaskIdTaskNameResponse")
    @RequestWrapper(localName = "updateTaskByTaskIdTaskName", targetNamespace = "http://iendpoint.api.tm.alekseev.ru/", className = "ru.alekseev.tm.api.iendpoint.UpdateTaskByTaskIdTaskName")
    @ResponseWrapper(localName = "updateTaskByTaskIdTaskNameResponse", targetNamespace = "http://iendpoint.api.tm.alekseev.ru/", className = "ru.alekseev.tm.api.iendpoint.UpdateTaskByTaskIdTaskNameResponse")
    public void updateTaskByTaskIdTaskName(
        @WebParam(name = "arg0", targetNamespace = "")
        ru.alekseev.tm.api.iendpoint.SessionDTO arg0,
        @WebParam(name = "arg1", targetNamespace = "")
        java.lang.String arg1,
        @WebParam(name = "arg2", targetNamespace = "")
        java.lang.String arg2
    );

    @WebMethod
    @Action(input = "http://iendpoint.api.tm.alekseev.ru/ITaskEndpoint/findAllTasksByUserIdRequest", output = "http://iendpoint.api.tm.alekseev.ru/ITaskEndpoint/findAllTasksByUserIdResponse")
    @RequestWrapper(localName = "findAllTasksByUserId", targetNamespace = "http://iendpoint.api.tm.alekseev.ru/", className = "ru.alekseev.tm.api.iendpoint.FindAllTasksByUserId")
    @ResponseWrapper(localName = "findAllTasksByUserIdResponse", targetNamespace = "http://iendpoint.api.tm.alekseev.ru/", className = "ru.alekseev.tm.api.iendpoint.FindAllTasksByUserIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<ru.alekseev.tm.api.iendpoint.TaskDTO> findAllTasksByUserId(
        @WebParam(name = "arg0", targetNamespace = "")
        ru.alekseev.tm.api.iendpoint.SessionDTO arg0
    );

    @WebMethod
    @Action(input = "http://iendpoint.api.tm.alekseev.ru/ITaskEndpoint/deleteTaskRequest", output = "http://iendpoint.api.tm.alekseev.ru/ITaskEndpoint/deleteTaskResponse")
    @RequestWrapper(localName = "deleteTask", targetNamespace = "http://iendpoint.api.tm.alekseev.ru/", className = "ru.alekseev.tm.api.iendpoint.DeleteTask")
    @ResponseWrapper(localName = "deleteTaskResponse", targetNamespace = "http://iendpoint.api.tm.alekseev.ru/", className = "ru.alekseev.tm.api.iendpoint.DeleteTaskResponse")
    public void deleteTask(
        @WebParam(name = "arg0", targetNamespace = "")
        java.lang.String arg0
    );

    @WebMethod
    @Action(input = "http://iendpoint.api.tm.alekseev.ru/ITaskEndpoint/deleteTaskByTaskIdRequest", output = "http://iendpoint.api.tm.alekseev.ru/ITaskEndpoint/deleteTaskByTaskIdResponse")
    @RequestWrapper(localName = "deleteTaskByTaskId", targetNamespace = "http://iendpoint.api.tm.alekseev.ru/", className = "ru.alekseev.tm.api.iendpoint.DeleteTaskByTaskId")
    @ResponseWrapper(localName = "deleteTaskByTaskIdResponse", targetNamespace = "http://iendpoint.api.tm.alekseev.ru/", className = "ru.alekseev.tm.api.iendpoint.DeleteTaskByTaskIdResponse")
    public void deleteTaskByTaskId(
        @WebParam(name = "arg0", targetNamespace = "")
        ru.alekseev.tm.api.iendpoint.SessionDTO arg0,
        @WebParam(name = "arg1", targetNamespace = "")
        java.lang.String arg1
    );

    @WebMethod
    @Action(input = "http://iendpoint.api.tm.alekseev.ru/ITaskEndpoint/addTaskByUserIdTaskNameRequest", output = "http://iendpoint.api.tm.alekseev.ru/ITaskEndpoint/addTaskByUserIdTaskNameResponse")
    @RequestWrapper(localName = "addTaskByUserIdTaskName", targetNamespace = "http://iendpoint.api.tm.alekseev.ru/", className = "ru.alekseev.tm.api.iendpoint.AddTaskByUserIdTaskName")
    @ResponseWrapper(localName = "addTaskByUserIdTaskNameResponse", targetNamespace = "http://iendpoint.api.tm.alekseev.ru/", className = "ru.alekseev.tm.api.iendpoint.AddTaskByUserIdTaskNameResponse")
    public void addTaskByUserIdTaskName(
        @WebParam(name = "arg0", targetNamespace = "")
        ru.alekseev.tm.api.iendpoint.SessionDTO arg0,
        @WebParam(name = "arg1", targetNamespace = "")
        java.lang.String arg1
    );
}
