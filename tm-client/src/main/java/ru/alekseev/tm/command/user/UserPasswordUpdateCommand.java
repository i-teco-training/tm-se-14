package ru.alekseev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.alekseev.tm.api.iendpoint.Session;
import ru.alekseev.tm.api.iendpoint.SessionDTO;
import ru.alekseev.tm.command.system.AbstractCommand;

public final class UserPasswordUpdateCommand extends AbstractCommand {
    @Override
    @NotNull
    public final String getName() {
        return "password-update";
    }

    @Override
    @NotNull
    public final String getDescription() {
        return "Change USER password";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[PASSWORD UPDATING]");
        SessionDTO currentSession = serviceLocator.getCurrentSession();
        if (currentSession == null) return;
        if (currentSession.getUserId() == null || currentSession.getUserId().isEmpty()) return;

        System.out.println("ENTER NEW PASSWORD");
        @NotNull final String newPassword = serviceLocator.getTerminalService().getFromConsole();

        serviceLocator.getUserEndpointService().getUserEndpointPort().updateUserPassword(currentSession, newPassword);
        System.out.println("[OK]");
    }

    @Override
    public final boolean isSecure() {
        return true;
    }
}
