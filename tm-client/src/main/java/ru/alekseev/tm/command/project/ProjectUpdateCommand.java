package ru.alekseev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.alekseev.tm.api.iendpoint.IProjectEndpoint;
import ru.alekseev.tm.api.iendpoint.Session;
import ru.alekseev.tm.api.iendpoint.SessionDTO;
import ru.alekseev.tm.command.system.AbstractCommand;

public final class ProjectUpdateCommand extends AbstractCommand {
    @Override
    @NotNull
    public final String getName() {
        return "update-project";
    }

    @Override
    @NotNull
    public final String getDescription() {
        return "Update project";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[UPDATING OF PROJECT]");
        @NotNull final IProjectEndpoint projectEndpoint =
                serviceLocator.getProjectEndpointService().getProjectEndpointPort();

        SessionDTO currentSession = serviceLocator.getCurrentSession();
        if (currentSession == null) return;
        if (currentSession.getUserId() == null || currentSession.getUserId().isEmpty()) return;

        System.out.println("ENTER PROJECT ID");
        @NotNull final String projectId = serviceLocator.getTerminalService().getFromConsole();
        if (projectId.isEmpty()) {
            System.out.println("invalid input!");
            return;
        }

        System.out.println("ENTER NEW NAME");
        @NotNull final String projectName = serviceLocator.getTerminalService().getFromConsole();
        if (projectName.isEmpty()) {
            System.out.println("invalid input!");
            return;
        }

        projectEndpoint.updateProjectByProjectIdProjectName(currentSession,projectId, projectName);
        System.out.println("[OK]");
    }

    @Override
    public final boolean isSecure() {
        return true;
    }
}
