package ru.alekseev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.api.iendpoint.Session;
import ru.alekseev.tm.api.iendpoint.SessionDTO;
import ru.alekseev.tm.api.iendpoint.User;
import ru.alekseev.tm.command.system.AbstractCommand;

public final class UserLoadCurrentProfileCommand extends AbstractCommand {
    @Override
    @NotNull
    public final String getName() {
        return "load-profile";
    }

    @Override
    @NotNull
    public final String getDescription() {
        return "Load your Project Manager profile";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[LOADING YOUR PROFILE]");
        SessionDTO currentSession = serviceLocator.getCurrentSession();
        if (currentSession == null) return;
        if (currentSession.getUserId() == null || currentSession.getUserId().isEmpty()) return;

        @Nullable final User currentUser =
                serviceLocator.getUserEndpointService().getUserEndpointPort().findOneUser(currentSession);
        if (currentUser == null) return;
        System.out.println("Your login: " + currentUser.getLogin());
        System.out.println("Your id: " + currentUser.getId());
        System.out.println("Your account type: " + currentUser.getRoleType().toString());
        System.out.println("[OK]");
    }

    @Override
    public final boolean isSecure() {
        return true;
    }
}
