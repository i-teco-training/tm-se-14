package ru.alekseev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.api.iendpoint.ITaskEndpoint;
import ru.alekseev.tm.api.iendpoint.Session;
import ru.alekseev.tm.api.iendpoint.SessionDTO;
import ru.alekseev.tm.api.iendpoint.Task;
import ru.alekseev.tm.command.system.AbstractCommand;

public final class TaskUpdateCommand extends AbstractCommand {
    @Override
    @NotNull
    public final String getName() {
        return "update-task";
    }

    @Override
    @NotNull
    public final String getDescription() {
        return "Update task";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[UPDATING OF TASK]");
        @NotNull final ITaskEndpoint taskEndpoint =
                serviceLocator.getTaskEndpointService().getTaskEndpointPort();

        SessionDTO currentSession = serviceLocator.getCurrentSession();
        if (currentSession == null) return;
        if (currentSession.getUserId() == null || currentSession.getUserId().isEmpty()) return;

        System.out.println("ENTER TASK ID");
        @NotNull final String taskId = serviceLocator.getTerminalService().getFromConsole();
        if (taskId.isEmpty()) {
            System.out.println("invalid input!");
            return;
        }

        System.out.println("ENTER NEW NAME");
        @NotNull final String taskName = serviceLocator.getTerminalService().getFromConsole();
        if (taskId.isEmpty()) {
            System.out.println("invalid input!");
            return;
        }

        taskEndpoint.updateTaskByTaskIdTaskName(currentSession, taskId, taskName);
        System.out.println("[OK]");
    }

    @Override
    public final boolean isSecure() {
        return true;
    }
}
