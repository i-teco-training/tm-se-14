package ru.alekseev.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.enumerated.RoleType;

import javax.persistence.*;
import java.util.List;

@Setter
@Getter
@Entity
@Table(name = "app_user")
@NoArgsConstructor
public final class User extends AbstractEntity{
    @Nullable
    @Column(unique = true)
    private String login;

    @Nullable
    @Column(name = "passwordHash")
    private String passwordHashcode;

    @NotNull
    @Column(name = "role")
    @Enumerated(value = EnumType.STRING)
    private RoleType roleType = RoleType.USER;

    @Nullable
    private String email;

    @OneToMany(mappedBy = "user", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private List<Session> sessions;

    @OneToMany(mappedBy = "user", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private List<Project> projects;

    @OneToMany(mappedBy = "user", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private List<Task> tasks;
}
