package ru.alekseev.tm.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.api.ServiceLocator;
import ru.alekseev.tm.api.iendpoint.IDomainEndpoint;
import ru.alekseev.tm.dto.Domain;
import ru.alekseev.tm.dto.SessionDTO;
import ru.alekseev.tm.entity.Session;
import ru.alekseev.tm.entity.User;
import ru.alekseev.tm.util.ConvertUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Setter
@Getter
@NoArgsConstructor
@WebService(endpointInterface = "ru.alekseev.tm.api.iendpoint.IDomainEndpoint")
public class DomainEndpoint implements IDomainEndpoint {
    private ServiceLocator serviceLocator;

    public DomainEndpoint(ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    @Nullable
    @WebMethod
    public Domain getDomain(@WebParam @NotNull final SessionDTO sessionDTO) {
        @Nullable final User user = serviceLocator.getUserService().findOne(sessionDTO.getUserId());
        if (user == null) return null;
        @Nullable final Session session = ConvertUtil.convertDtoToSession(sessionDTO, user);
        if (!serviceLocator.getSessionService().isValid(session)) {
            //serviceLocator.getSessionService().delete(session.getId());
            return null;
        }
        return serviceLocator.getDomainService().getDomain();
    }

    @Override
    @WebMethod
    public void setDomain(
            @WebParam @NotNull final SessionDTO sessionDTO,
            @WebParam @NotNull final Domain domain
    ) {
        @Nullable final User user = serviceLocator.getUserService().findOne(sessionDTO.getUserId());
        if (user == null) return;
        @Nullable final Session session = ConvertUtil.convertDtoToSession(sessionDTO, user);
        if (!serviceLocator.getSessionService().isValid(session)) {
            //serviceLocator.getSessionService().delete(session.getId());
            return;
        }
        serviceLocator.getDomainService().setDomain(domain);
    }
}
