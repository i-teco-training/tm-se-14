package ru.alekseev.tm.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.alekseev.tm.api.ServiceLocator;
import ru.alekseev.tm.api.iendpoint.ISessionEndpoint;
import ru.alekseev.tm.dto.SessionDTO;
import ru.alekseev.tm.entity.Session;
import ru.alekseev.tm.entity.User;
import ru.alekseev.tm.util.ConvertUtil;
import ru.alekseev.tm.util.HashUtil;
import ru.alekseev.tm.util.SignatureUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Setter
@Getter
@NoArgsConstructor
@WebService(endpointInterface = "ru.alekseev.tm.api.iendpoint.ISessionEndpoint")
public class SessionEndpoint implements ISessionEndpoint {
    private ServiceLocator serviceLocator;

    public SessionEndpoint(ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

//    @Override
//    @WebMethod
//    public SessionDTO openSession(
//            @WebParam @NotNull final String login,
//            @WebParam @NotNull final String password
//    ) {
//        String passwordHashcode = HashUtil.getMd5(password);
//        User user = serviceLocator.getUserService().findOneByLoginAndPassword(login, passwordHashcode);
//        if (user == null) return null;
//        Session session = new Session();
//        session.setUser(user);
//        String signature = SignatureUtil.sign(session);
//        session.setSignature(signature);
//
//        serviceLocator.getSessionService().persist(session);
//        return ConvertUtil.convertSessionToDto(session);
//    }

    @Override
    @WebMethod
    public SessionDTO openSession(
            @WebParam @NotNull final String login,
            @WebParam @NotNull final String password
    ) {
        String passwordHashcode = HashUtil.getMd5(password);
        User user = serviceLocator.getUserService().findOneByLoginAndPassword(login, passwordHashcode);
        if (user == null) return null;
        Session session = new Session();
        session.setUser(user);

        SessionDTO sessionDTO = ConvertUtil.convertSessionToDto(session);
        String signature = SignatureUtil.sign(sessionDTO);

        session.setSignature(signature);

        serviceLocator.getSessionService().persist(session);
        return sessionDTO = ConvertUtil.convertSessionToDto(session);
    }

//    @Override
//    @WebMethod
//    public boolean isSessionValid(@WebParam @NotNull final SessionDTO sessionDTO) {
//        @Nullable final User user = serviceLocator.getUserService().findOne(sessionDTO.getUserId());
//        if (user == null) return false;
//        @Nullable final Session session = ConvertUtil.convertDtoToSession(sessionDTO, user);
//        return serviceLocator.getSessionService().isValid(session);
//    }
//
    @Override
    @WebMethod
    public void closeSession(@WebParam @NotNull final String sessionId) {
        serviceLocator.getSessionService().delete(sessionId);
    }
}
