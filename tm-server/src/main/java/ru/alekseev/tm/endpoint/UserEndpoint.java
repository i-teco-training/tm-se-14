package ru.alekseev.tm.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.api.ServiceLocator;
import ru.alekseev.tm.api.iendpoint.IUserEndpoint;
import ru.alekseev.tm.dto.SessionDTO;
import ru.alekseev.tm.dto.UserDTO;
import ru.alekseev.tm.entity.Session;
import ru.alekseev.tm.entity.User;
import ru.alekseev.tm.enumerated.RoleType;
import ru.alekseev.tm.util.ConvertUtil;
import ru.alekseev.tm.util.HashUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Setter
@Getter
@NoArgsConstructor
@WebService(endpointInterface = "ru.alekseev.tm.api.iendpoint.IUserEndpoint")
public class UserEndpoint implements IUserEndpoint {
    private ServiceLocator serviceLocator;

    public UserEndpoint(ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    @WebMethod
    public User findOneUserByLoginAndPassword(
            @WebParam @NotNull final String login,
            @WebParam @NotNull final String passwordHashcode
    ) {
        return serviceLocator.getUserService().findOneByLoginAndPassword(login, passwordHashcode);
    }

    @Override//////+
    @Nullable
    @WebMethod
    public final User findOneUser(@WebParam @NotNull final SessionDTO sessionDTO) {
        @Nullable final User user = serviceLocator.getUserService().findOne(sessionDTO.getUserId());
        if (user == null) return null;
        @Nullable final Session session = ConvertUtil.convertDtoToSession(sessionDTO, user);
        if (!serviceLocator.getSessionService().isValid(session)) {
            //serviceLocator.getSessionService().delete(session.getId());
            return null;
        }
        return serviceLocator.getUserService().findOne(session.getId());
    }

//    @Override
//    @WebMethod
//    public void addUser(@WebParam @NotNull final UserDTO entity) {
//
//        serviceLocator.getUserService().add(entity);
//    }

    @Override//+
    @WebMethod
    public void addUserByLoginAndPassword(
            @WebParam @NotNull final String login,
            @WebParam @NotNull final String password
    ) {
        String passwordHashcode = HashUtil.getMd5(password);
        serviceLocator.getUserService().addByLoginAndPassword(login, passwordHashcode);
    }

    @Override//initUsers
    @WebMethod
    public void addUserByLoginPasswordUserRole(
            @WebParam @NotNull final String login,
            @WebParam @NotNull final String passwordHashcode,
            @WebParam @NotNull final RoleType roleType
    ) {
        serviceLocator.getUserService().addByLoginPasswordUserRole(login, passwordHashcode, roleType);
    }

//    @Override
//    @WebMethod
//    public void updateUser(@WebParam @NotNull final UserDTO entity) {
//        serviceLocator.getUserService().update(entity);
//    }

    @Override
    @WebMethod
    public void updateUserPassword(@WebParam @NotNull final SessionDTO sessionDTO, @WebParam @NotNull final String password) {
        @Nullable final User user = serviceLocator.getUserService().findOne(sessionDTO.getUserId());
        if (user == null) return;
        @Nullable final Session session = ConvertUtil.convertDtoToSession(sessionDTO, user);
        if (!serviceLocator.getSessionService().isValid(session)) {
            //serviceLocator.getSessionService().delete(session.getId());
            return;
        }
        String passwordHashcode = HashUtil.getMd5(password);
        serviceLocator.getUserService().updateUserPassword(session.getUser().getId(), passwordHashcode);
    }

    @Override
    @WebMethod
    public void updateUserByRole(@WebParam @NotNull final SessionDTO sessionDTO, @WebParam @NotNull final RoleType roleType) {
        @Nullable final User user = serviceLocator.getUserService().findOne(sessionDTO.getUserId());
        if (user == null) return;
        @Nullable final Session session = ConvertUtil.convertDtoToSession(sessionDTO, user);
        if (!serviceLocator.getSessionService().isValid(session)) {
            //serviceLocator.getSessionService().delete(session.getId());
            return;
        }
            serviceLocator.getUserService().updateUserByRole(session.getUser().getId(), roleType);
    }

//    @Override
//    @WebMethod
//    public void deleteUser(@WebParam @NotNull final String id) {
//        serviceLocator.getUserService().delete(id);
//    }

    @Override
    @WebMethod
    public void deleteUserByLoginAndPassword(
            @WebParam @NotNull final String login,
            @WebParam @NotNull final String passwordHashcode
    ) {
        serviceLocator.getUserService().deleteByLoginAndPassword(login, passwordHashcode);
    }
}
