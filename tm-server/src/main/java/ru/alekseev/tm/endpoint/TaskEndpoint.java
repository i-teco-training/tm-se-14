package ru.alekseev.tm.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.api.ServiceLocator;
import ru.alekseev.tm.api.iendpoint.ITaskEndpoint;
import ru.alekseev.tm.dto.SessionDTO;
import ru.alekseev.tm.dto.TaskDTO;
import ru.alekseev.tm.entity.Session;
import ru.alekseev.tm.entity.Task;
import ru.alekseev.tm.entity.User;
import ru.alekseev.tm.util.ConvertUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
@WebService(endpointInterface = "ru.alekseev.tm.api.iendpoint.ITaskEndpoint")
public class TaskEndpoint implements ITaskEndpoint {
    private ServiceLocator serviceLocator;

    public TaskEndpoint(ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    @WebMethod
    public TaskDTO findOneTaskByUserIdAndTaskId(@NotNull final String userId, @NotNull final String taskId) {
        Task task = serviceLocator.getTaskService().findOneByUserIdAndTaskId(userId, taskId);
        return ConvertUtil.convertTaskToDto(task);
    }

    @Override//нужный
    @Nullable
    @WebMethod
    public List<TaskDTO> findAllTasksByUserId(@WebParam @NotNull final SessionDTO sessionDTO) {
        @Nullable final User user = serviceLocator.getUserService().findOne(sessionDTO.getUserId());
        if (user == null) return null;
        @Nullable final Session session = ConvertUtil.convertDtoToSession(sessionDTO, user);
        if (!serviceLocator.getSessionService().isValid(session)) {
            //serviceLocator.getSessionService().delete(session.getId());
            return null;
        }
        List<Task> tasks = serviceLocator.getTaskService().findAllByUserId(sessionDTO.getUserId());
        List<TaskDTO> taskDTOList = new ArrayList<>();
        for (Task task : tasks) {
            taskDTOList.add(ConvertUtil.convertTaskToDto(task));
        }
        return taskDTOList;
    }

    @Override//нужный
    @WebMethod
    public void addTaskByUserIdTaskName(
            @WebParam @NotNull final SessionDTO sessionDTO,
            @WebParam @NotNull final String taskName
    ) {
        @Nullable final User user = serviceLocator.getUserService().findOne(sessionDTO.getUserId());
        if (user == null) return;
        @Nullable final Session session = ConvertUtil.convertDtoToSession(sessionDTO, user);
        if (!serviceLocator.getSessionService().isValid(session)) {
            //serviceLocator.getSessionService().delete(session.getId());
            return;
        }
        @NotNull final Task task = new Task();
        task.setName(taskName);
        task.setUser(user);
        serviceLocator.getTaskService().add(task);
    }

    @Override//нужный
    @WebMethod
    public void updateTaskByTaskIdTaskName(
            @WebParam @NotNull final SessionDTO sessionDTO,
            @WebParam @NotNull final String taskId,
            @WebParam @NotNull final String name
    ) {
        @Nullable final User user = serviceLocator.getUserService().findOne(sessionDTO.getUserId());
        if (user == null) return;
        @Nullable final Session session = ConvertUtil.convertDtoToSession(sessionDTO, user);
        if (!serviceLocator.getSessionService().isValid(session)) {
            //serviceLocator.getSessionService().delete(session.getId());
            return;
        }
        serviceLocator.getTaskService().updateByNewData(user, taskId, name);
    }

    @Override//нужный
    @WebMethod
    public void deleteTaskByTaskId(
            @WebParam @NotNull final SessionDTO sessionDTO,
            @WebParam @NotNull final String taskId
    ) {
        @Nullable final User user = serviceLocator.getUserService().findOne(sessionDTO.getUserId());
        if (user == null) return;
        @Nullable final Session session = ConvertUtil.convertDtoToSession(sessionDTO, user);
        if (!serviceLocator.getSessionService().isValid(session)) {
            //serviceLocator.getSessionService().delete(session.getId());
            return;
        }
        serviceLocator.getTaskService().deleteByUserIdAndTaskId(sessionDTO.getUserId(), taskId);
    }

    @Override
    @WebMethod
    public void deleteTask(@WebParam @NotNull final String id) {
        serviceLocator.getTaskService().delete(id);
    }
}
