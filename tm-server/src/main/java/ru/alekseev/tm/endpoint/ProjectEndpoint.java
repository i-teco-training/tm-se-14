package ru.alekseev.tm.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.api.ServiceLocator;
import ru.alekseev.tm.api.iendpoint.IProjectEndpoint;
import ru.alekseev.tm.dto.ProjectDTO;
import ru.alekseev.tm.dto.SessionDTO;
import ru.alekseev.tm.entity.Project;
import ru.alekseev.tm.entity.Session;
import ru.alekseev.tm.entity.User;
import ru.alekseev.tm.util.ConvertUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
@WebService(endpointInterface = "ru.alekseev.tm.api.iendpoint.IProjectEndpoint")
public class ProjectEndpoint implements IProjectEndpoint {
    private ServiceLocator serviceLocator;

    public ProjectEndpoint(ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    @WebMethod
    @Nullable
    public ProjectDTO findOneProjectByUserIdAndProjectId(
            @WebParam @NotNull final String userId,
            @WebParam @NotNull final String projectId
    ) {
        Project project = serviceLocator.getProjectService().findOneByUserIdAndProjectId(userId, projectId);
        return ConvertUtil.convertProjectToDto(project);
    }

    @Override//нужный
    @WebMethod
    @Nullable
    public List<ProjectDTO> findAllProjectsByUserId(@WebParam @NotNull final SessionDTO sessionDTO) {
        @Nullable final User user = serviceLocator.getUserService().findOne(sessionDTO.getUserId());
        if (user == null) return null;
        @Nullable final Session session = ConvertUtil.convertDtoToSession(sessionDTO, user);
        if (!serviceLocator.getSessionService().isValid(session)) {
            //serviceLocator.getSessionService().delete(session.getId());
            return null;
        }
        List<Project> projects = serviceLocator.getProjectService().findAllByUserId(sessionDTO.getUserId());
        List<ProjectDTO> projectDTOList = new ArrayList<>();
        for (Project project : projects) {
            projectDTOList.add(ConvertUtil.convertProjectToDto(project));
        }
        return projectDTOList;
    }

    @Override//нужный
    @WebMethod
    public void addProjectByUserIdProjectName(
            @WebParam @NotNull final SessionDTO sessionDTO,
            @WebParam @NotNull final String projectName
    ) {
        @Nullable final User user = serviceLocator.getUserService().findOne(sessionDTO.getUserId());
        if (user == null) return;
        @Nullable final Session session = ConvertUtil.convertDtoToSession(sessionDTO, user);
        if (!serviceLocator.getSessionService().isValid(session)) {
            //serviceLocator.getSessionService().delete(session.getId());
            return;
        }
        @NotNull final Project project = new Project();
        project.setName(projectName);
        project.setUser(user);
        serviceLocator.getProjectService().add(project);
    }

    @Override//нужный
    @WebMethod
    public void updateProjectByProjectIdProjectName(
            @WebParam @NotNull final SessionDTO sessionDTO,
            @WebParam @NotNull final String projectId,
            @WebParam @NotNull final String name
    ) {
        @Nullable final User user = serviceLocator.getUserService().findOne(sessionDTO.getUserId());
        if (user == null) return;
        @Nullable final Session session = ConvertUtil.convertDtoToSession(sessionDTO, user);
        if (!serviceLocator.getSessionService().isValid(session)) {
            //serviceLocator.getSessionService().delete(session.getId());
            return;
        }
        serviceLocator.getProjectService().updateByUserProjectIdProjectName(user, projectId, name);
    }

    @Override//нужный
    @WebMethod
    public void deleteProjectByProjectId(
            @WebParam @NotNull final SessionDTO sessionDTO,
            @WebParam @NotNull final String projectId
    ) {
        @Nullable final User user = serviceLocator.getUserService().findOne(sessionDTO.getUserId());
        if (user == null) return;
        @Nullable final Session session = ConvertUtil.convertDtoToSession(sessionDTO, user);
        if (!serviceLocator.getSessionService().isValid(session)) {
            //serviceLocator.getSessionService().delete(session.getId());
            return;
        }
        serviceLocator.getProjectService().deleteByProjectId(sessionDTO.getUserId(), projectId);
    }

}
