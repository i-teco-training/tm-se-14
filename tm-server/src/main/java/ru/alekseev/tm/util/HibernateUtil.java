package ru.alekseev.tm.util;

import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.entity.Project;
import ru.alekseev.tm.entity.Session;
import ru.alekseev.tm.entity.Task;
import ru.alekseev.tm.entity.User;

import javax.persistence.EntityManagerFactory;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class HibernateUtil {
    public static EntityManagerFactory factory() {
        @Nullable String driver = "";
        @Nullable String url = "";
        @Nullable String username = "";
        @Nullable String password = "";

        @NotNull final Properties properties = new Properties();
        try {
            final InputStream fis = HibernateUtil.class.getClassLoader().getResourceAsStream("hibernate.properties");
            properties.load(fis);
        } catch (IOException e) {
            e.printStackTrace();
        }

        final Map<String, String> settings = new HashMap<>();
        settings.put(Environment.DRIVER, properties.getProperty("driver"));
        settings.put(Environment.URL, properties.getProperty("url"));
        settings.put(Environment.USER, properties.getProperty("username"));
        settings.put(Environment.PASS, properties.getProperty("password"));
        settings.put(Environment.DIALECT, "org.hibernate.dialect.MySQL5InnoDBDialect");
        settings.put(Environment.HBM2DDL_AUTO, "update");
        settings.put(Environment.SHOW_SQL, "true");
        final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        final StandardServiceRegistry registry = registryBuilder.build();
        final MetadataSources sources = new MetadataSources(registry);
        sources.addAnnotatedClass(Task.class);
        sources.addAnnotatedClass(Project.class);
        sources.addAnnotatedClass(User.class);
        sources.addAnnotatedClass(Session.class);
        final Metadata metadata = sources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }
}
