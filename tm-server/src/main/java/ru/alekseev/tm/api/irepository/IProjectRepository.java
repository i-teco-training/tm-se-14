package ru.alekseev.tm.api.irepository;

import ru.alekseev.tm.entity.Project;
import ru.alekseev.tm.entity.User;

import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    Project findOneByUserIdAndProjectId(String userId, String projectId);

    List<Project> findAllByUserId(String userId);

    void updateByUserProjectIdProjectName(User user, String projectId, String projectName);

    void deleteByUserId(String userId);

    void deleteProjectByProjectId(String userId, String projectId);
}
