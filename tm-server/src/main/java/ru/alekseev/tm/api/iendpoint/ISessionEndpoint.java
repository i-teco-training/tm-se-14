package ru.alekseev.tm.api.iendpoint;

import ru.alekseev.tm.dto.SessionDTO;

import javax.jws.WebService;

@WebService
public interface ISessionEndpoint {

    SessionDTO openSession(String login, String passwordHashcode);

//    boolean isSessionValid(SessionDTO sessionDTO) ;
//
    void closeSession(String sessionId);
}
