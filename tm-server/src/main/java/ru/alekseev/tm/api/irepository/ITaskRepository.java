package ru.alekseev.tm.api.irepository;

import ru.alekseev.tm.entity.Task;
import ru.alekseev.tm.entity.User;

import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    Task findOneByUserIdAndTaskId(String userId, String taskId);

    List<Task> findAllByUserId(String userId);

//    void clearByProjectId(String projectId);
//
//    void clearByUserIdAndProjectId(String userId, String projectId);

    void deleteByUserIdAndTaskId(String userId, String taskId);

    void updateByNewData(User user, String taskId, String name);
}
