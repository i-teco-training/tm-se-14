package ru.alekseev.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class SessionDTO extends AbstractDTO {
    @NotNull
    private Long timestamp = new Date().getTime();

    @Nullable
    //@Column(name = "user_id")
    private String userId;

    @Nullable
    private String signature;
}
