package ru.alekseev.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.api.ServiceLocator;
import ru.alekseev.tm.api.iservice.*;
import ru.alekseev.tm.endpoint.*;
import ru.alekseev.tm.enumerated.RoleType;
import ru.alekseev.tm.service.*;
import ru.alekseev.tm.util.HashUtil;
import ru.alekseev.tm.util.HibernateUtil;

import javax.persistence.EntityManagerFactory;
import javax.xml.ws.Endpoint;

public final class Bootstrap implements ServiceLocator {
    @Nullable
    private final EntityManagerFactory entityManagerFactory = HibernateUtil.factory();
    @NotNull private final IProjectService projectService = new ProjectService(entityManagerFactory);
    @NotNull private final ITaskService taskService = new TaskService(entityManagerFactory);
    @NotNull private final IUserService userService = new UserService(entityManagerFactory);
    @NotNull private final ISessionService sessionService = new SessionService(entityManagerFactory);
    @NotNull private final IDomainService domainService = new DomainService(entityManagerFactory);

    @Override
    @NotNull
    public final IProjectService getProjectService() {
        return projectService;
    }

    @Override
    @NotNull
    public final ITaskService getTaskService() {
        return taskService;
    }

    @Override
    @NotNull
    public final IUserService getUserService() {
        return userService;
    }

    @Override
    @NotNull
    public ISessionService getSessionService() {
        return sessionService;
    }

    @Override
    @NotNull
    public final IDomainService getDomainService() {
        return domainService;
    }

    @NotNull public final static String PROJECT_ENDPOINT_WSDL = "http://localhost:8080/ProjectEndpoint?wsdl";
    @NotNull public final static String SESSION_ENDPOINT_WSDL = "http://localhost:8080/SessionEndpoint?wsdl";
    @NotNull public final static String TASK_ENDPOINT_WSDL = "http://localhost:8080/TaskEndpoint?wsdl";
    @NotNull public final static String USER_ENDPOINT_WSDL = "http://localhost:8080/UserEndpoint?wsdl";
    @NotNull public final static String DOMAIN_ENDPOINT_WSDL = "http://localhost:8080/DomainEndpoint?wsdl";

    public final void start() {
        try {
            initUsers();
            initEndpoints();
        } catch (Exception e) {
            System.out.println("exception was thrown");
            e.printStackTrace();
        }
    }

    public final void initUsers() {
        if (userService.findOneByLoginAndPassword("qqq", HashUtil.getMd5("qqq"))!= null) return;
        if (userService.findOneByLoginAndPassword("www", HashUtil.getMd5("www"))!= null) return;

        this.userService.addByLoginPasswordUserRole("qqq", HashUtil.getMd5("qqq"), RoleType.ADMIN);
        this.userService.addByLoginPasswordUserRole("www", HashUtil.getMd5("www"), RoleType.USER);
    }

    public final void initEndpoints() {
        ProjectEndpoint projectEndpoint = new ProjectEndpoint(this);
        TaskEndpoint taskEndpoint = new TaskEndpoint(this);
        UserEndpoint userEndpoint = new UserEndpoint(this);
        SessionEndpoint sessionEndpoint = new SessionEndpoint(this);
        DomainEndpoint domainEndpoint = new DomainEndpoint(this);

        Endpoint.publish(PROJECT_ENDPOINT_WSDL, projectEndpoint);
        Endpoint.publish(TASK_ENDPOINT_WSDL, taskEndpoint);
        Endpoint.publish(USER_ENDPOINT_WSDL, userEndpoint);
        Endpoint.publish(SESSION_ENDPOINT_WSDL, sessionEndpoint);
        Endpoint.publish(DOMAIN_ENDPOINT_WSDL, domainEndpoint);
    }
}