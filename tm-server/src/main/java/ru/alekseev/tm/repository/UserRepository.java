package ru.alekseev.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.api.irepository.IUserRepository;
import ru.alekseev.tm.entity.User;
import ru.alekseev.tm.enumerated.RoleType;

import javax.persistence.EntityManager;
import java.util.List;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {
    @Nullable private EntityManager entityManager;
    public UserRepository(@Nullable EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Nullable
    @Override
    @SneakyThrows
    public final User findOne(@NotNull String id) {
        @NotNull final User user = entityManager.find(User.class, id);
        return user;
    }

    @Override
    @Nullable
    @SneakyThrows
    public final User findOneByLoginAndPasswordHashcode(
            @NotNull final String login,
            @NotNull final String passwordHashcode
    ) {
        return entityManager.createQuery("SELECT a FROM User a WHERE a.login=:login AND a.passwordHashcode = :passwordHashcode", User.class)////******
                .setParameter("login", login)
                .setParameter("passwordHashcode", passwordHashcode)
                .setMaxResults(1)
                .getSingleResult();
    }

    @Override
    @SneakyThrows
    public final void persist(@NotNull final User user) {
        entityManager.persist(user);
    }

    @Override
    @SneakyThrows
    public final void merge(@NotNull User user) {
        entityManager.merge(user);
    }

    @Override
    @SneakyThrows
    public final void updateUserPassword(@NotNull final String userId, @NotNull final String passwordHashcode) {
        @NotNull final User user = entityManager.find(User.class, userId);
        user.setPasswordHashcode(passwordHashcode);
        entityManager.merge(user);
    }

    @Override
    @SneakyThrows
    public final void updateUserByRole(@NotNull final String userId, @NotNull final RoleType roleType) {
        @NotNull final User user = entityManager.find(User.class, userId);
        user.setRoleType(roleType);
        entityManager.merge(user);
    }

    @Override
    @SneakyThrows
    public final void deleteByLoginAndPassword(
            @NotNull final String login,
            @NotNull final String passwordHashcode
    ) {
        entityManager.createQuery("DELETE FROM User a WHERE a.login=:login AND a.passwordHashcode=:passwordHashcode", User.class)
                .setParameter("login", login)
                .setParameter("passwordHashcode", passwordHashcode)
                .executeUpdate();
    }
}
