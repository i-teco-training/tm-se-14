package ru.alekseev.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.api.irepository.ITaskRepository;
import ru.alekseev.tm.entity.Project;
import ru.alekseev.tm.entity.Task;
import ru.alekseev.tm.entity.User;
import ru.alekseev.tm.enumerated.Status;

import javax.persistence.EntityManager;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {
    @Nullable private EntityManager entityManager;
    public TaskRepository(@Nullable EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    @Nullable
    @SneakyThrows
    public final Task findOneByUserIdAndTaskId(@NotNull final String userId, @NotNull final String taskId) {
        return entityManager.createQuery("SELECT a FROM Task a WHERE a.user.id=:userId AND a.id = :taskId", Task.class)
                .setParameter("userId", userId)
                .setParameter("taskId", taskId)
                .setMaxResults(1)
                .getSingleResult();
    }

    @Override
    @NotNull
    @SneakyThrows
    public final List<Task> findAllByUserId(@NotNull final String userId) {
        return entityManager.createQuery("SELECT a FROM Task a WHERE a.user.id=:userId", Task.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    @SneakyThrows
    public final void persist(@NotNull final Task task) {
        entityManager.persist(task);
    }

    @Override
    @SneakyThrows
    public final void updateByNewData(
            @NotNull final User user,
            @NotNull final String taskId,
            @NotNull final String name
    ) {
        @NotNull final Task task = new Task();
        task.setUser(user);
        task.setId(taskId);
        task.setName(name);
        entityManager.merge(task);
    }

    @Override
    @SneakyThrows
    public void delete(@NotNull String id) {
        entityManager.remove(entityManager.find(Task.class, id));
    }

    @Override
    @SneakyThrows
    public final void deleteByUserIdAndTaskId(@NotNull final String userId, @NotNull final String taskId) {
        entityManager.createQuery("DELETE FROM Task a WHERE a.user.id=:userId AND a.id=:taskId", Task.class)
                .setParameter("userId", userId)
                .setParameter("taskId", taskId)
                .executeUpdate();
    }

//    @Override
//    @SneakyThrows
//    public final void clearByProjectId(@NotNull final String projectId) {
//        @NotNull final String query = "DELETE FROM app_task WHERE project_id = ?";
//        PreparedStatement preparedStatement = connection.prepareStatement(query);
//        preparedStatement.setString(1, projectId);
//        preparedStatement.execute();
//        preparedStatement.close();
//    }
//
//    @Override
//    @SneakyThrows
//    public final void clearByUserIdAndProjectId(@NotNull final String userId, @NotNull final String projectId) {
//        @NotNull final String query = "DELETE FROM app_task WHERE user_id = ? AND project_id = ?";
//        PreparedStatement preparedStatement = connection.prepareStatement(query);
//        preparedStatement.setString(1, userId);
//        preparedStatement.setString(2, projectId);
//        preparedStatement.execute();
//        preparedStatement.close();
//    }
}
