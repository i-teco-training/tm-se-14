package ru.alekseev.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.api.irepository.IProjectRepository;
import ru.alekseev.tm.entity.Project;
import ru.alekseev.tm.entity.User;

import javax.persistence.EntityManager;
import java.sql.PreparedStatement;
import java.util.List;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {
    @Nullable private EntityManager entityManager;
    public ProjectRepository(@Nullable EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project findOne(@NotNull String id) {
        @Nullable final Project project = entityManager.find(Project.class, id);
        return project;
    }

    @Override
    @Nullable
    @SneakyThrows
    public final Project findOneByUserIdAndProjectId(@NotNull final String userId, @NotNull final String projectId) {
        return entityManager.createQuery("SELECT a FROM Project a WHERE a.user.id=:userId AND a.id = :projectId", Project.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .setMaxResults(1)
                .getSingleResult();
    }

    @Override
    @NotNull
    @SneakyThrows
    public final List<Project> findAllByUserId(@NotNull final String userId) {
        return entityManager.createQuery("SELECT a FROM Project a WHERE a.user.id=:userId", Project.class)
                .setParameter("userId", userId)
                .getResultList();
    }


    @Override
    @SneakyThrows
    public final void persist(@NotNull final Project project) {
        entityManager.persist(project);
    }

    @Override
    @SneakyThrows
    public final void updateByUserProjectIdProjectName(
            @NotNull final User user,
            @NotNull final String projectId,
            @NotNull final String projectName
    ) {
        @NotNull final Project project = new Project();
        project.setName(projectName);
        project.setUser(user);
        project.setId(projectId);
        entityManager.merge(project);
    }

    @Override//нужный
    @SneakyThrows
    public void delete(@NotNull String id) {
        entityManager.remove(entityManager.find(Project.class, id));
    }

    @Override
    @SneakyThrows
    public final void deleteByUserId(@NotNull final String userId) {
        entityManager.createQuery("DELETE FROM Project a WHERE a.user.id=:userId", Project.class)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    @SneakyThrows
    public final void deleteProjectByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        entityManager.createQuery("DELETE FROM Project a WHERE a.user.id=:userId AND a.id=:projectId", Project.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .executeUpdate();
    }
}
