package ru.alekseev.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.api.irepository.ISessionRepository;
import ru.alekseev.tm.entity.Session;

import javax.persistence.EntityManager;

public class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {
    @Nullable private EntityManager entityManager;
    public SessionRepository(@Nullable EntityManager entityManager) {
        this.entityManager = entityManager;
    }

//    @Override
//    @SneakyThrows
//    public Session findOne(String sessionId) {
//        return entityManager.find(Session.class, sessionId);
//    }

    @Override
    @SneakyThrows
    public void persist(Session session) {
        entityManager.persist(session);
    }

    @Override
    @SneakyThrows
    public void delete(String sessionId) {
        entityManager.remove(entityManager.find(Session.class, sessionId));
    }
}
