package ru.alekseev.tm.service;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.api.irepository.IUserRepository;
import ru.alekseev.tm.api.iservice.IUserService;
import ru.alekseev.tm.entity.User;
import ru.alekseev.tm.enumerated.RoleType;
import ru.alekseev.tm.repository.UserRepository;
import ru.alekseev.tm.util.HibernateUtil;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.List;

@Getter
@Setter
public class UserService extends AbstractService<User> implements IUserService {
    private EntityManagerFactory entityManagerFactory;

    public UserService(EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
    }

    @Nullable
    public final User findOne(@NotNull final String id) {
        @Nullable final EntityManager entityManager = entityManagerFactory.createEntityManager();
        if (entityManager == null) return null;
        IUserRepository userRepository = new UserRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            @Nullable User user = userRepository.findOne(id);
            entityManager.getTransaction().commit();
            return user;
        } catch (Exception e) {
            e.printStackTrace();
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
        return null;
    }

    @Override
    @Nullable
    public final User findOneByLoginAndPassword(@NotNull final String login, @NotNull final String passwordHashcode) {
        @Nullable final EntityManager entityManager = entityManagerFactory.createEntityManager();
        if (entityManager == null) return null;
        IUserRepository userRepository = new UserRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            @Nullable User user = userRepository.findOneByLoginAndPasswordHashcode(login,passwordHashcode);
            entityManager.getTransaction().commit();
            return user;
        } catch (Exception e) {
            e.printStackTrace();
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
        return null;
    }

    @Override
    public final List<User> findAll() {
        return super.findAll();
    }

    @Override
    public final void add(@NotNull final User user) {
        @Nullable final EntityManager entityManager = entityManagerFactory.createEntityManager();
        if (entityManager == null) return;
        IUserRepository userRepository = new UserRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            userRepository.persist(user);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public final void addByLoginPasswordUserRole(
            @NotNull final String login,
            @NotNull final String passwordHashcode,
            @NotNull final RoleType roleType
    ) {
        if (login.isEmpty() || passwordHashcode.isEmpty() || roleType.toString().isEmpty()) return;
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHashcode(passwordHashcode);
        user.setRoleType(roleType);
        add(user);
    }

    @Override
    public final void addByLoginAndPassword(
            @NotNull final String login,
            @NotNull final String passwordHashcode
    ) {
        if (login.isEmpty() || passwordHashcode.isEmpty()) return;
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHashcode(passwordHashcode);
        add(user);
    }

    @Override
    public final void update(@NotNull final User user) {
        @Nullable final EntityManager entityManager = entityManagerFactory.createEntityManager();
        if (entityManager == null) return;
        IUserRepository userRepository = new UserRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            userRepository.merge(user);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void updateUserPassword(@NotNull final String userId, @NotNull final String passwordHashcode) {
        @Nullable final EntityManager entityManager = entityManagerFactory.createEntityManager();
        if (entityManager == null) return;
        IUserRepository userRepository = new UserRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            userRepository.updateUserPassword(userId, passwordHashcode);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void updateUserByRole(String userId, RoleType roleType) {
        @Nullable final EntityManager entityManager = entityManagerFactory.createEntityManager();
        if (entityManager == null) return;
        IUserRepository userRepository = new UserRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            userRepository.updateUserByRole(userId, roleType);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

//    @Override
//    public final void delete(@NotNull final String id) {
//        @Nullable final EntityManager entityManager = entityManagerFactory.createEntityManager();
//        if (entityManager == null) return;
//        IUserRepository userRepository = new UserRepository(entityManager);
//        try {
//            userRepository.delete(id);
//            entityManager.getTransaction().commit();
//        } catch (Exception e) {
//            e.printStackTrace();
//            entityManager.getTransaction().rollback();
//        } finally {
//            entityManager.close();
//        }
//    }

    @Override
    public final void deleteByLoginAndPassword(@NotNull final String login, @NotNull final String passwordHashcode) {
        @Nullable final EntityManager entityManager = entityManagerFactory.createEntityManager();
        if (entityManager == null) return;
        IUserRepository userRepository = new UserRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            userRepository.deleteByLoginAndPassword(login, passwordHashcode);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }
}
