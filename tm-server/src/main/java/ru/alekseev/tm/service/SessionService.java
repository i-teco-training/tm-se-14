package ru.alekseev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.api.irepository.ISessionRepository;
import ru.alekseev.tm.api.iservice.ISessionService;
import ru.alekseev.tm.dto.SessionDTO;
import ru.alekseev.tm.entity.Session;
import ru.alekseev.tm.entity.User;
import ru.alekseev.tm.repository.SessionRepository;
import ru.alekseev.tm.util.ConvertUtil;
import ru.alekseev.tm.util.SignatureUtil;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

public final class SessionService extends AbstractService<Session> implements ISessionService {
    private EntityManagerFactory entityManagerFactory;

    public SessionService(EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
    }

    @Override
    public final void persist(@NotNull final Session session) {
        @Nullable final EntityManager entityManager = entityManagerFactory.createEntityManager();
        if (entityManager == null) return;
        @NotNull final ISessionRepository sessionRepository = new SessionRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            sessionRepository.persist(session);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public final boolean isValid(@Nullable final Session session) {
        if (session == null) return false;
        if (session.getSignature() == null || session.getSignature().isEmpty()) return false;
        if (session.getUser() == null || session.getUser().getId().isEmpty()) return false;
        if (session.getTimestamp() == null) return false;

        @NotNull final String sourceSignature = session.getSignature();

        SessionDTO sessionDTO = ConvertUtil.convertSessionToDto(session);
        sessionDTO.setSignature(null);
        @NotNull final String targetSignature = SignatureUtil.sign(sessionDTO);
        return sourceSignature.equals(targetSignature);
    }

    public void delete(String id) {
        @Nullable final EntityManager entityManager = entityManagerFactory.createEntityManager();
        if (entityManager == null) return;
        @NotNull final ISessionRepository sessionRepository = new SessionRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            sessionRepository.delete(id);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }
}
