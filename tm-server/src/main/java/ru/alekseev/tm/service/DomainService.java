package ru.alekseev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.api.irepository.IProjectRepository;
import ru.alekseev.tm.api.irepository.ITaskRepository;
import ru.alekseev.tm.api.irepository.IUserRepository;
import ru.alekseev.tm.api.iservice.IDomainService;
import ru.alekseev.tm.dto.Domain;
import ru.alekseev.tm.entity.Project;
import ru.alekseev.tm.entity.Task;
import ru.alekseev.tm.entity.User;
import ru.alekseev.tm.repository.ProjectRepository;
import ru.alekseev.tm.repository.TaskRepository;
import ru.alekseev.tm.repository.UserRepository;
import ru.alekseev.tm.util.HibernateUtil;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.List;

public final class DomainService implements IDomainService {
    private EntityManagerFactory entityManagerFactory;

    public DomainService(EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
    }

    @Override
    @Nullable
    public final Domain getDomain() {
        @Nullable final EntityManager entityManager = entityManagerFactory.createEntityManager();
        if (entityManager == null) return null;
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            @Nullable final List<Project> projects = projectRepository.findAll();
            @Nullable final List<Task> tasks = taskRepository.findAll();
            @Nullable final List<User> users = userRepository.findAll();
            @NotNull final Domain domain = new Domain();
            domain.setProjects(projects);
            domain.setTasks(tasks);
            domain.setUsers(users);
            entityManager.getTransaction().commit();
            return domain;
        } catch (Exception e) {
            e.printStackTrace();
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
        return null;
    }

    @Override
    public final void setDomain(@Nullable final Domain domain) {
        @Nullable final EntityManager entityManager = entityManagerFactory.createEntityManager();
        if (entityManager == null) return;
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            projectRepository.clear();
            projectRepository.addAll(domain.getProjects());
            taskRepository.clear();
            taskRepository.addAll(domain.getTasks());
            userRepository.clear();
            userRepository.addAll(domain.getUsers());
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }
}
