package ru.alekseev.tm.service;

import ru.alekseev.tm.api.iservice.IService;
import ru.alekseev.tm.entity.AbstractEntity;

import java.util.List;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {
    @Override
    public void add(E entity) {

    }

    @Override
    public List<E> findAll() {
        return null;
    }

    @Override
    public void update(E entity) {

    }

    @Override
    public void delete(String id) {

    }

    @Override
    public void clear() {

    }
}
