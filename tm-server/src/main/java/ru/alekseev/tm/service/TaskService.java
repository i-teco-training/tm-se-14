package ru.alekseev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.api.irepository.ITaskRepository;
import ru.alekseev.tm.api.iservice.ITaskService;
import ru.alekseev.tm.entity.Task;
import ru.alekseev.tm.entity.User;
import ru.alekseev.tm.repository.TaskRepository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.List;

public class TaskService extends AbstractService<Task> implements ITaskService {
    private EntityManagerFactory entityManagerFactory;

    public TaskService(EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
    }

    @Override
    @Nullable
    public final Task findOneByUserIdAndTaskId(@NotNull final String userId, @NotNull final String taskId) {
        @Nullable final EntityManager entityManager = entityManagerFactory.createEntityManager();
        if (entityManager == null) return null;
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            @Nullable final Task task = taskRepository.findOneByUserIdAndTaskId(userId, taskId);
            entityManager.getTransaction().commit();
            return task;
        } catch (Exception e) {
            e.printStackTrace();
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
        return null;
    }

    @Override
    @Nullable
    public final List<Task> findAllByUserId(@NotNull final String userId) {
        @Nullable final EntityManager entityManager = entityManagerFactory.createEntityManager();
        if (entityManager == null) return null;
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        @Nullable List<Task> tasks = null;
        try {
            entityManager.getTransaction().begin();
            tasks = taskRepository.findAllByUserId(userId);
            entityManager.getTransaction().commit();
            return tasks;
        } catch (Exception e) {
            e.printStackTrace();
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
        return tasks;
    }

    @Override//нужный
    public final void add(@NotNull final Task task) {
        @Nullable final EntityManager entityManager = entityManagerFactory.createEntityManager();
        if (entityManager == null) return;
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            taskRepository.persist(task);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

//    @Override
//    public final void addTaskByUserIdTaskName(@NotNull final String userId, @NotNull final String taskName) {
//        @NotNull final Task task = new Task();
//        task.setUserId(userId);
//        task.setName(taskName);
//        add(task);
//    }

    @Override
    public final void updateByNewData(
            @NotNull final User user,
            @NotNull final String taskId,
            @NotNull final String name
    ) {
        if (user == null || taskId.isEmpty() || name.isEmpty()) return;
        @Nullable final EntityManager entityManager = entityManagerFactory.createEntityManager();
        if (entityManager == null) return;
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            taskRepository.updateByNewData(user, taskId, name);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public final void delete(@NotNull final String id) {
        if (id.isEmpty()) return;
        @Nullable final EntityManager entityManager = entityManagerFactory.createEntityManager();
        if (entityManager == null) return;
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            taskRepository.delete(id);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public final void deleteByUserIdAndTaskId(@NotNull final String userId, @NotNull final String taskId) {
        if (userId.isEmpty() || taskId.isEmpty()) return;
        @Nullable final EntityManager entityManager = entityManagerFactory.createEntityManager();
        if (entityManager == null) return;
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            taskRepository.deleteByUserIdAndTaskId(userId, taskId);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }
}
