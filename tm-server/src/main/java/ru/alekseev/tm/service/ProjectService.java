package ru.alekseev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.api.irepository.IProjectRepository;
import ru.alekseev.tm.api.iservice.IProjectService;
import ru.alekseev.tm.entity.Project;
import ru.alekseev.tm.entity.User;
import ru.alekseev.tm.repository.ProjectRepository;
import ru.alekseev.tm.util.HibernateUtil;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.List;

public final class ProjectService extends AbstractService<Project> implements IProjectService {
    private EntityManagerFactory entityManagerFactory;

    public ProjectService(EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
    }

    @Override
    @Nullable
    public final Project findOneByUserIdAndProjectId(@NotNull final String userId, @NotNull final String projectId) {
        @Nullable final EntityManager entityManager = entityManagerFactory.createEntityManager();
        if (entityManager == null) return null;
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            @Nullable final Project project = projectRepository.findOneByUserIdAndProjectId(userId, projectId);
            entityManager.getTransaction().commit();
            return project;
        } catch (Exception e) {
            e.printStackTrace();
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
        return null;
    }



    @Override//нужный
    @Nullable
    public final List<Project> findAllByUserId(@NotNull final String userId) {
        if (userId.isEmpty()) return null;
        @Nullable final EntityManager entityManager = entityManagerFactory.createEntityManager();
        if (entityManager == null) return null;
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        @Nullable List<Project> projects = null;
        try {
            entityManager.getTransaction().begin();
            projects = projectRepository.findAllByUserId(userId);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
        return projects;
    }

    @Override//нужный
    public final void add(@NotNull final Project project) {
        @Nullable final EntityManager entityManager = entityManagerFactory.createEntityManager();
        if (entityManager == null) return;
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            projectRepository.persist(project);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override//пока не нужен
    public void addProjectByUserIdProjectName(String userId, String projectName) {
        @NotNull final Project project = new Project();
        @NotNull final User user = new User();
        user.setId(userId);
        project.setUser(user);
        project.setName(projectName);
        add(project);
    }

    @Override//нужный
    public final void updateByUserProjectIdProjectName(
            @NotNull final User user,
            @NotNull final String projectId,
            @NotNull final String projectName
    ) {
        if (user == null || projectId.isEmpty() || projectName.isEmpty()) return;
        @Nullable final EntityManager entityManager = entityManagerFactory.createEntityManager();
        if (entityManager == null) return;
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            projectRepository.updateByUserProjectIdProjectName(user, projectId, projectName);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override//нужный
    public final void delete(@NotNull final String id) {
        @Nullable final EntityManager entityManager = entityManagerFactory.createEntityManager();
        if (entityManager == null) return;
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            projectRepository.delete(id);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override//нужный
    public final void deleteByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        if (userId.isEmpty() || projectId.isEmpty()) return;
        @Nullable final EntityManager entityManager = entityManagerFactory.createEntityManager();
        if (entityManager == null) return;
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            @Nullable Project projectForExistenceChecking = projectRepository.findOne(projectId);
            if (projectForExistenceChecking.getUser().getId() == null ||
                    projectForExistenceChecking.getUser().getId().isEmpty())
                return;
            if (!userId.equals(projectForExistenceChecking.getUser().getId())) return;
            delete(projectId);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

//    @Override
//    public final void clearByUserId(@NotNull final String userId) {
//        if (userId.isEmpty()) return;
//        @Nullable final EntityManager entityManager = entityManagerFactory.createEntityManager();
//        if (entityManager == null) return;
//        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
//        try {
//            projectRepository.deleteByUserId(userId);
//            entityManager.getTransaction().commit();
//        } catch (Exception e) {
//            e.printStackTrace();
//            entityManager.getTransaction().rollback();
//        } finally {
//            entityManager.close();
//        }
//    }
}
